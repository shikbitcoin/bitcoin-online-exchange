/**
 * Created by Hangyuan on 2017/10/21.
 */
var mongoose = require('mongoose');
mongoose.set('debug', true);
mongoose.connect("mongodb://Harry:a64554376@cluster0-shard-00-00-mp06g.mongodb.net:27017," +
                 "cluster0-shard-00-01-mp06g.mongodb.net:27017," +
                 "cluster0-shard-00-02-mp06g.mongodb.net:27017/bitcoins" +
                 "?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin", function () {
    var db = mongoose.connection;
    db.on("error", console.error.bind(console, "connection error:"));
    db.once("open", function callback(){
        console.log("CONNECTED");
    });
});

module.exports = mongoose;

