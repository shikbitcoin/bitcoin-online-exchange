import argparse
import datetime
import dateutil
import candle
from pymongo import MongoClient

epoch = datetime.datetime.utcfromtimestamp(0)


def unix_time_millis(dt):
    return (dt - epoch).total_seconds() * 1000.0


if __name__ == '__main__':
    client = MongoClient("mongodb://Harry:a64554376@cluster0-shard-00-00-mp06g.mongodb.net:27017,"
                         "cluster0-shard-00-01-mp06g.mongodb.net:27017,"
                         "cluster0-shard-00-02-mp06g.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0"
                         "&authSource=admin")

    parser = argparse.ArgumentParser()
    parser.add_argument('currency',
                        choices=['LTC-EUR', 'LTC-BTC', 'BTC-GBP', 'BTC-EUR', 'ETH-EUR', 'ETH-BTC', 'LTC-USD',
                                 'BTC-USD', 'ETH-USD'], help='Choose from [\'LTC-EUR\',\'LTC-BTC\', \'BTC-GBP\', '
                                                             '\'BTC-EUR\', \'ETH-EUR\', \'ETH-BTC\', \'LTC-USD\','
                                                             '\'BTC-USD\', \'ETH-USD\']')
    args = parser.parse_args()
    currency = args.currency
    dbName = 'bitcoins'
    collection_name = currency.replace(currency[3], '_') + '_candle_data'
    print(collection_name)
    db = client[dbName]
    collections = db[collection_name]
    last_dict = collections.find_one(sort=[("time", -1)])
    print(last_dict)
    last_date = dateutil.parser.parse(last_dict.get("time"))
    print(last_date)
    update_list = candle.data_loading(last_date, currency)
    # url = candle.url_combiner(currency, last_date.strftime('%Y-%m-%dT%H:%M:%S', time.localtime(last_date_epoch)))
    # update_list = candle.get_data(url)
    print("The length of update list is " + str(len(update_list)))
    print(update_list)
    update_date = dateutil.parser.parse(update_list[0].get("time"))
    update_first_date = int(unix_time_millis(update_date))
    final_update_list = []
    i = 0
    last_date_database = int(unix_time_millis(last_date))
    if not (update_first_date == last_date_database):
        while int(unix_time_millis(dateutil.parser.parse(update_list[i].get("time")))) > last_date_database and i < len(
                update_list) - 1:
            final_update_list.append(update_list[i])
            i = i + 1
            print("i: " + str(i))
        print("The length of final update list is " + str(len(final_update_list)))
        print(final_update_list)
        collections.insert_many(final_update_list)
        print(str(len(final_update_list)) + " documents inserted")
    else:
        print("Already updated")
