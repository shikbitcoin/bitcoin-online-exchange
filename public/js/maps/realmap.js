/**
 * This example uses pulsating circles CSS by Kevin Urrutia
 * http://kevinurrutia.tumblr.com/post/16411271583/creating-a-css3-pulsating-circle
 */
var pointMap = [];
var point = [];
var test = [];
var i=0;

function generateData(){
  jQuery.get( "https://bitnodes.21.co/api/v1/snapshots/latest/",null,function(data){

    var nodes = data.nodes;
    /*
    for (var d in nodes){
      pointMap.push({
        "zoomLevel": 5,
        "scale": 0.5,
        "title": d[6],
        "latitude": d[7],
        "longitude": d[8]
      });
      */

      for (var d in nodes){
        pointMap.push({
          "zoomLevel": 5,
          "scale": 0.5,
          "title": nodes[d][6],
          "latitude": Number(nodes[d][8]),
          "longitude": Number(nodes[d][9])
        });
      }

      for(i=0;i<100;i++){
        point.push(pointMap[i]);
      }
      test.push(point[1]);

  },"json" );

  test.push({
    "zoomLevel": 5,
    "scale": 0.5,
    "title": "Frankfurt",
    "latitude": 50.1167,
    "longitude": 8.6833
  });
  console.log(point);
  //console.log(test);
}

generateData();

var map = AmCharts.makeChart( "chartdiv", {
  "type": "map",
  "projection": "miller",

  "imagesSettings": {
    "rollOverColor": "#089282",
    "rollOverScale": 3,
    "selectedScale": 3,
    "selectedColor": "#089282",
    "color": "#13564e"
  },

  "areasSettings": {
    "unlistedAreasColor": "#15A892"
  },

	"dataProvider": {
		"map": "worldLow",
		"images": point
	}

} );



// add events to recalculate map position when the map is moved or zoomed
map.addListener( "positionChanged", updateCustomMarkers );

// this function will take current images on the map and create HTML elements for them
function updateCustomMarkers( event ) {
  // get map object
  var map = event.chart;

  // go through all of the images
  for ( var x in map.dataProvider.images ) {
    // get MapImage object
    var image = map.dataProvider.images[ x ];

    // check if it has corresponding HTML element
    if ( 'undefined' == typeof image.externalElement )
      image.externalElement = createCustomMarker( image );

    // reposition the element accoridng to coordinates
    var xy = map.coordinatesToStageXY( image.longitude, image.latitude );
    image.externalElement.style.top = xy.y + 'px';
    image.externalElement.style.left = xy.x + 'px';
  }
}

// this function creates and returns a new marker element
function createCustomMarker( image ) {
  // create holder
  var holder = document.createElement( 'div' );
  holder.className = 'map-marker';
  holder.title = image.title;
  holder.style.position = 'absolute';

  // maybe add a link to it?
  if ( undefined != image.url ) {
    holder.onclick = function() {
      window.location.href = image.url;
    };
    holder.className += ' map-clickable';
  }

  // create dot
  var dot = document.createElement( 'div' );
  dot.className = 'dot';
  holder.appendChild( dot );

  // create pulse
  var pulse = document.createElement( 'div' );
  pulse.className = 'pulse';
  holder.appendChild( pulse );

  // append the marker to the map container
  image.chart.chartDiv.appendChild( holder );

  return holder;
}

setInterval( function() {

  if(i<pointMap.length){
      for(var j = 0; j<100;j++){
        point.shift();
        point.push(pointMap[i+j]);
        i++;
      }
  }
  else
  {
    i=0;
    for(var j = 0; j<100;j++){
      point.shift();
      point.push(pointMap[i+j]);
      i++;
    }
  }
  console.log(point);
  map.validateData();
}, 2000 );
