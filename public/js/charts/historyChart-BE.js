
$(function () {
    if ($('#line-chart')[0]) {
		var chart = AmCharts.makeChart( "line-chart", {
		  "type": "stock",
		  "theme": "dark",

			"dataSets": [ {
		    "title": "BTC/EUR",
		    "fieldMappings": [ {
		      "fromField": "open",
		      "toField": "open"
		    }, {
		      "fromField": "high",
		      "toField": "high"
		    }, {
		      "fromField": "low",
		      "toField": "low"
		    }, {
		      "fromField": "close",
		      "toField": "close"
		    }, {
		      "fromField": "volume",
		      "toField": "volume"
		    } ],
		    "compared": false,
		    "categoryField": "_id",

		    /**
		     * data loader for data set data
		     */
		    "dataLoader": {
		      "url": "/BTC_EUR_candle_data.json",
		      "format": "json",
		      "showCurtain": true,
		      "showErrors": true,
		      "async": true,
		      "reverse": true,
		      "delimiter": ",",
		      "useColumnNames": true
		    }

		  }],

		  "dataDateFormat": "YYYY-MM-DD",

		  "panels": [ {
		     // "title": "Value",
		      "percentHeight": 70,

		      "stockGraphs": [ {
		        "type": "candlestick",
		        "id": "g1",
				"balloonText": "Open:<b>[[open]]</b><br>Low:<b>[[low]]</b><br>High:<b>[[high]]</b><br>Close:<b>[[close]]</b><br>",

		        "openField": "open",
		        "closeField": "close",
		        "highField": "high",
		        "lowField": "low",
		        "valueField": "close",

		        "lineColor": "#fff",
		        "fillColors": "#fff",
		        "negativeLineColor": "#db4c3c",
		        "negativeFillColors": "#db4c3c",
		        "fillAlphas": 1,
		        "comparedGraphLineThickness": 2,
		        "columnWidth": 0.7,
		        "useDataSetColors": false,
		        "comparable": true,
		        "compareField": "close",
		        "showBalloon": true,
		        "proCandlesticks": true
		      } ],

		      "stockLegend": {
		        "valueTextRegular": undefined,
		        "periodValueTextComparing": "[[percents.value.close]]%"
		      },

			  "chartCursor": {
				  "color": "#fff",
				  "cursorColor": "#ff0000",
				  "valueLineEnabled": true,
				  "valueLineBalloonEnabled": true,
                  "categoryBalloonDateFormat": "YYYY-MM-DD"
			  }

		  } ],

		  "panelsSettings": {
		    "color": "#fff",
		    "plotAreaFillColors": "#333",
		    "plotAreaFillAlphas": 0.6,
		    "marginLeft": 60,
		    "marginTop": 5,
		    "marginBottom": 5
		  },

		  "chartScrollbarSettings": {
		    "graph": "g1",
		    "graphType": "line",
			  "dragIconHeight": "0",
        "dragIconWidth": "0",
		    "backgroundColor": "#333",
		    "graphFillColor": "#666",
		    "graphFillAlpha": 0.5,
		    "gridColor": "#555",
		    "gridAlpha": 0.6,
		    "selectedBackgroundColor": "#444",
			"selectedGraphFillAlpha": 0.6
		  },

		  "categoryAxesSettings": {
		    "equalSpacing": true,
		    "gridColor": "#555",
		    "gridAlpha": 1,
              "minPeriod": "DD",
              "groupToPeriods": ["DD"]
		  },

		  "valueAxesSettings": {
		    "gridColor": "#555",
		    "gridAlpha": 1,
		    "inside": false,
		    "showLastLabel": true
		  },

		  "chartCursorSettings": {
		    "pan": true,
		    "valueLineEnabled": true,
		    "valueLineBalloonEnabled": true
		  },

		  "legendSettings": {
		    "color": "#fff"
		  },

            "periodSelector": {
                "position": "bottom",
                "periods": [{
                    "period": "MM",
                    "count": 1,
                    "label": "1 Month"
                },{
                    "period": "MM",
                    "count": 3,
                    "selected": true,
                    "label": "3 Month"
                }, {
                    "period": "MM",
                    "count": 6,
                    "label": "6 Months"
                }, {
                    "period": "YYYY",
                    "count": 1,
                    "label": "1 Year"
                }
                ]
            }

        } );

        $("#line-chart").bind("plothover", function (event, pos, item) {
            if (item) {
                var x = item.datapoint[0].toFixed(2),
                    y = item.datapoint[1].toFixed(2);
                $("#linechart-tooltip").html(item.series.label + " of " + x + " = " + y).css({top: item.pageY+5, left: item.pageX+5}).fadeIn(200);
            }
            else {
                $("#linechart-tooltip").hide();
            }
        });

        $("<div id='linechart-tooltip' class='chart-tooltip'></div>").appendTo("body");
    }

});
