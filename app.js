var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');

var result = require('./app/routes/routes.js');
var fs = require('fs');
var app = express();

app.set('views', path.join(__dirname,'app','views'));
app.use(express.static(path.join(__dirname, '/public')));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/',result);

app.listen(3000, function () {
    console.log('Bitcoin app listening on port 3000!')
});

module.exports = app;