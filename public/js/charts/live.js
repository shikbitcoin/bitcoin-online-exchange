/**
 * Generate random chart data
 */
var chartData1 = [];
var chartData2 = [];
var chartData3 = [];

function generateChartData() {
  jQuery.get( "https://api.gdax.com/products/BTC-USD/trades",null,function(data){

    //console.log(data);
    for(i=data.length-1;i>=0;i--){
      chartData1.push({
        date: new Date(data[i].time),
        value: Number(data[i].price),
        volume: Number(data[i].size)
      });
    }
    jQuery.get( "https://api.gdax.com/products/ETH-USD/trades",null,function(data){

        //console.log(data);
        for(i=data.length-1;i>=0;i--){
          chartData2.push({
            date: new Date(data[i].time),
            value: Number(data[i].price),
            volume: Number(data[i].size)
          });
        }
        jQuery.get( "https://api.gdax.com/products/LTC-USD/trades",null,function(data){

          //console.log(data);
          for(i=data.length-1;i>=0;i--){
            chartData3.push({
              date: new Date(data[i].time),
              value: Number(data[i].price),
              volume: Number(data[i].size)
            });
          }
          console.log(chartData1);
          console.log(chartData2);
          console.log(chartData3);
          //chart.validateData();
          //chart.validateData();
          },"json" );

          chart.validateData();
        // console.log(processData);
        //chart.validateData();
        },"json" );
    //console.log(chartData1);
    //chart.validateData();
    },"json" );
};

generateChartData();

/**
 * Create the chart
 */
var chart = AmCharts.makeChart( "liveChartDiv", {
  "type": "stock",
  "theme": "dark",

  // This will keep the selection at the end across data updates
  "glueToTheEnd": true,

  // Defining data sets
    "dataSets": [ {
      "title": "Bitcoin",
      "fieldMappings": [ {
        "fromField": "value",
        "toField": "value"
      }, {
        "fromField": "volume",
        "toField": "volume"
      } ],
      "dataProvider": chartData1,
      "categoryField": "date"
    }, {
      "title": "Ethereum",
      "fieldMappings": [ {
        "fromField": "value",
        "toField": "value"
      }, {
        "fromField": "volume",
        "toField": "volume"
      } ],
      "dataProvider": chartData2,
      "categoryField": "date"
    }, {
      "title": "Litecoin",
      "fieldMappings": [ {
        "fromField": "value",
        "toField": "value"
      }, {
        "fromField": "volume",
        "toField": "volume"
      } ],
      "dataProvider": chartData3,
      "categoryField": "date"
    } ],

  "dataDateFormat": "YYYY-MM-DD JJ:NN:SS",
  // Panels
  "panels": [ {
      "showCategoryAxis": true,
	  "stockGraphs": [ {
      "id": "g1",
      "valueField": "value",
	  "balloonText": "[[title]]:<b> $[[value]]</b>",
	  "bullet": "bubble",
	  "bulletSize": 5,
      "comparable": true,
      "compareField": "value",
	  "compareGraphBalloonText": "[[title]]:<b> $[[value]]</b>",
	  "compareGraphBullet": "round",
	  "compareGraphBulletSize": 5,
    } ],
    "stockLegend": {
		"switchable": false,
		"valueTextRegular": "$[[value]]"
	},
  } ],

  "panelsSettings": {
	"color": "#333",
	"plotAreaFillColors": "#000",
	"plotAreaFillAlphas": 0.5,
	"marginLeft": 60,
	"marginTop": 5,
	"marginBottom": 5
  },

  "chartScrollbarSettings": {
	  "enabled": false
  },

  "categoryAxesSettings": {
	"dashLength": 5,
    "axisColor": "#fff",
    "color": "#fff",
    "fillAlpha": 0,
    "equalSpacing": true,
    "gridColor": "#fff",
    "gridAlpha": 0.2,
	"labelFrequency": 2,
	"minPeriod": "10ss",
	"groupToPeriods": ["10ss"],
	"equalSpacing": true
  },

  "valueAxesSettings": {
	"dashLength": 5,
	"axisColor": "#fff",
    "color": "#fff",
    "fillAlpha": 0,
    "gridColor": "#fff",
    "gridAlpha": 0.2,
    "inside": false,
    "showLastLabel": true,
    "usePrefixes": true,
    "unit": "$",
    "unitPosition": "left",
	"labelFrequency": 2
  },

  "chartCursorSettings": {
    "valueBalloonsEnabled": true,
    "fullWidth": true,
    "cursorAlpha": 0.1,
    "valueLineBalloonEnabled": true,
    "valueLineEnabled": true,
    "valueLineAlpha": 0.5
  },

  "legendSettings": {
	"color": "#fff"
  },

  // Data Set Selector
  "dataSetSelector": {
    "position": "left",
	"selectText": "Choose a currency"
  }

} );

/**
 * Set up interval to update the data periodically
 */
setInterval( function() {
  chartData1.shift();
  chartData2.shift();
  chartData3.shift();
  jQuery.get( "https://api.gdax.com/products/BTC-USD/trades",null,function(data){

    //console.log(data);

      chartData1.push({
        date: new Date(data[0].time),
        value: Number(data[0].price),
        volume: Number(data[0].size)
      });

    jQuery.get( "https://api.gdax.com/products/ETH-USD/trades",null,function(data){

        //console.log(data);

          chartData2.push({
            date: new Date(data[0].time),
            value: Number(data[0].price),
            volume: Number(data[0].size)
          });

        jQuery.get( "https://api.gdax.com/products/LTC-USD/trades",null,function(data){

          //console.log(data);

            chartData3.push({
              date: new Date(data[0].time),
              value: Number(data[0].price),
              volume: Number(data[0].size)
            });

          console.log(chartData1);
          console.log(chartData2);
          console.log(chartData3);
          //chart.validateData();
          //chart.validateData();
          },"json" );
        // console.log(processData);
        //chart.validateData();
        },"json" );
    //console.log(chartData1);
    //chart.validateData();
    },"json" );
    chart.validateData();
}, 5000 );
