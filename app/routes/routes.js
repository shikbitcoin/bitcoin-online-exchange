/**
 * Created by Hangyuan on 2017/10/21.
 */

var express = require('express');
var controller = require('../controller/controller.js');
var router = express.Router();

/***************************************************
 *         These are the homepage routers.         *
 ***************************************************/
router.get('/', controller.showHomepage);
router.get('/index.html', controller.showHomepage);

/***************************************************
 *     These are the real time page routers.       *
 ***************************************************/
router.get('/realtime.html', controller.showRealTimePage);
router.get('/BTC-EUR.html', controller.showBTC_EUR_Page);
router.get('/BTC-GBP.html', controller.showBTC_GBP_Page);
router.get('/ETH-USD.html', controller.showETH_USD_Page);
router.get('/ETH-EUR.html', controller.showETH_EUR_Page);
router.get('/ETH-BTC.html', controller.showETH_BTC_Page);
router.get('/LTC-USD.html', controller.showLTC_USD_Page);
router.get('/LTC-EUR.html', controller.showLTC_EUR_Page);
router.get('/LTC-BTC.html', controller.showLTC_BTC_Page);

/***************************************************
 *       These are the history page routers.       *
 ***************************************************/
router.get('/history.html', controller.showHistory_Page);
router.get('/BTC-EUR-H.html', controller.showBTC_EUR_H_Page);
router.get('/BTC-GBP-H.html', controller.showBTC_GBP_H_Page);
router.get('/ETH-USD-H.html', controller.showETH_USD_H_Page);
router.get('/ETH-EUR-H.html', controller.showETH_EUR_H_Page);
router.get('/ETH-BTC-H.html', controller.showETH_BTC_H_Page);
router.get('/LTC-USD-H.html', controller.showLTC_USD_H_Page);
router.get('/LTC-EUR-H.html', controller.showLTC_EUR_H_Page);
router.get('/LTC-BTC-H.html', controller.showLTC_BTC_H_Page);

/***************************************************
 *     These are the comparison page routers.      *
 ***************************************************/
router.get('/comparison.html', controller.showComparisonPage);
router.get('/EUR-C.html', controller.showEUR_C_Page);

/***************************************************
 *          This is the map page router.           *
 ***************************************************/
router.get('/map.html', controller.showMapPage);

/***************************************************
 *      This is the latest news page router.       *
 ***************************************************/
router.get('/latest.html', controller.showLatestPage);

router.get('/BTC_USD_candle_data.json', controller.BTC_USD_HistoryData);
router.get('/BTC_EUR_candle_data.json', controller.BTC_EUR_HistoryData);
router.get('/BTC_GBP_candle_data.json', controller.BTC_GBP_HistoryData);
router.get('/ETH_USD_candle_data.json', controller.ETH_USD_HistoryData);
router.get('/ETH_EUR_candle_data.json', controller.ETH_EUR_HistoryData);
router.get('/ETH_BTC_candle_data.json', controller.ETH_BTC_HistoryData);
router.get('/LTC_USD_candle_data.json', controller.LTC_USD_HistoryData);
router.get('/LTC_EUR_candle_data.json', controller.LTC_EUR_HistoryData);
router.get('/LTC_BTC_candle_data.json', controller.LTC_BTC_HistoryData);
module.exports = router;
