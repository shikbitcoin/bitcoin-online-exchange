// generate some random data, quite different range
//DONE
var processData = [];
var i=0;

function refreshData(){
  jQuery.get( "https://api.gdax.com/products/LTC-USD/trades",null,function(data){

    //console.log(data);
    for(i=data.length-1;i>=0;i--){
      processData.push({
        date: new Date(data[i].time),
        price: Number(data[i].price)
      });
    }
    console.log(processData);
    chart.validateData();
    },"json" );
};

refreshData();


var chart = AmCharts.makeChart("liveChart", {
    "type": "serial",
    "theme": "light",
    "marginRight": 80,
    "dataProvider": processData,
    "valueAxes": [{
        "position": "left",
        "labelFrequency": 2,
        "dashLength": 5,

		"usePrefixes": true,
	    "unit": "$",
	    "unitPosition": "left",
		"axisColor": "#fff",
	    "color": "#fff",
	    "fillAlpha": 0,
	    "gridColor": "#fff",
	    "gridAlpha": 0.5,
    }],

    "graphs": [{
        "id": "g1",
		"lineColor": "#FAAF08",
        "fillAlphas": 0.4,
        "valueField": "price",
        "balloonText": "<div style='margin:5px;'>Price:<b>$[[price]]</b></div>"
    }],
    "chartScrollbar": {
        "enabled": false
    },
    "chartCursor": {
        "categoryBalloonDateFormat": "JJ:NN:SS, DD MMMM",
        "cursorPosition": "mouse",
		"color": "#fff",
		"cursorColor": "#fff",
		"cursorAlpha": 0.5,
		"categoryBalloonColor": "#f00",
		"valueLineEnabled": true,
  		"valueLineBalloonEnabled": true,
    },
    "categoryField": "date",
    "categoryAxis": {
        "minPeriod": "ss",
        "parseDates": true,
        "dashLength": 5,
		"axisColor": "#fff",
	    "color": "#fff",
	    "fillAlpha": 0,
	    "equalSpacing": true,
	    "gridColor": "#fff",
	    "gridAlpha": 0.5
    }
});

setInterval( function() {
  processData.shift();
  jQuery.get( "https://api.gdax.com/products/LTC-USD/trades",null,function(data){
      processData.push({
        date: new Date(data[0].time),
        price: Number(data[0].price)
      });
    console.log(processData);
    chart.validateData();
    },"json" );
}, 5000 );
