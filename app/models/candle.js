/**
 * Created by Hangyuan on 2017/10/21.
 */

var mongoose = require('./database.js');

var CandleSchema = new mongoose.Schema(
    {
        "time": Number,
        "low": Number,
        "high": Number,
        "open": Number,
        "close": Number,
        "volume": Number
    });

CandleSchema.statics.findHistoryData = function (callback) {
       return this.aggregate([{
           $group : {
               _id : {$substr:["$time", 0, 10]},
               low: { $min: "$low" },
               high: { $max: "$high" },
               open: { $first: "$open" },
               close: { $last: "$close"},
               volume: { $sum: "$volume"}
           }},
           {$sort: {_id:1}}
       ]).exec(callback);
};


var BTC_USD_candle_data = mongoose.model("BTC_USD_candle_data", CandleSchema, "BTC_USD_candle_data" );
var BTC_EUR_candle_data = mongoose.model("BTC_EUR_candle_data", CandleSchema, "BTC_EUR_candle_data" );
var BTC_GBP_candle_data = mongoose.model("BTC_GBP_candle_data", CandleSchema, "BTC_GBP_candle_data" );
var ETH_USD_candle_data = mongoose.model("ETH_USD_candle_data", CandleSchema, "ETH_USD_candle_data" );
var ETH_EUR_candle_data = mongoose.model("ETH_EUR_candle_data", CandleSchema, "ETH_EUR_candle_data" );
var ETH_BTC_candle_data = mongoose.model("ETH_BTC_candle_data", CandleSchema, "ETH_BTC_candle_data" );
var LTC_USD_candle_data = mongoose.model("LTC_USD_candle_data", CandleSchema, "LTC_USD_candle_data" );
var LTC_EUR_candle_data = mongoose.model("LTC_EUR_candle_data", CandleSchema, "LTC_EUR_candle_data" );
var LTC_BTC_candle_data = mongoose.model("LTC_BTC_candle_data", CandleSchema, "LTC_BTC_candle_data" );

module.exports = BTC_USD_candle_data;
module.exports = BTC_EUR_candle_data;
module.exports = BTC_GBP_candle_data;
module.exports = ETH_USD_candle_data;
module.exports = ETH_EUR_candle_data;
module.exports = ETH_BTC_candle_data;
module.exports = LTC_USD_candle_data;
module.exports = LTC_EUR_candle_data;
module.exports = LTC_BTC_candle_data;
