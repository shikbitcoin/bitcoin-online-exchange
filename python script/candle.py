import datetime
import time
from urllib.error import HTTPError
import os
from bs4 import BeautifulSoup
import json
import dateutil.parser

try:
    from urllib2 import urlopen
except ImportError:
    from urllib.request import urlopen
import argparse


def get_data(data_url):
    try:
        soup = BeautifulSoup(urlopen(data_url), "html.parser")
    except HTTPError:
        print('Too many requests, try again')
        time.sleep(2)
        soup = BeautifulSoup(urlopen(data_url), "html.parser")
    d = soup.get_text()
    json_data = json.loads(d)
    print(json_data)
    return json_data


def save_data(json_data, currency):
    fn = os.path.join(os.path.dirname(__file__), currency.replace(currency[3], '_')+'_candle_data.json')
    with open(fn, 'w') as outfile:
        # final_data = '{"'+currency.replace(currency[3], '_')+'":'+json.dumps(json_data)+'}'
        # final_json_data = json.loads(final_data)
        # json.dump(final_json_data, outfile)
        json.dump(json_data, outfile)
        print("Data saved")


def check_data(data):
    if type(data) == list:
        print("Correct data")
        return True
    else:
        print(data)
        return False


def url_combiner(currency, start_date, e_date, g):
    iso_s_date = datetime.datetime.isoformat(start_date)
    iso_e_date = datetime.datetime.isoformat(e_date)
    return 'https://api.gdax.com/products/' + currency + '/candles?start=' + iso_s_date + '&end=' + iso_e_date + '&granularity=' + g


def data_loading(start_date, currency):
    granularity = '60'
    key = ["time", "low", "high", "open", "close", "volume"]
    print(start_date)
    part_end_date_naive = start_date + datetime.timedelta(seconds=12000)
    print(part_end_date_naive)
    # tz = pytz.timezone('Australia/Sydney')
    # part_end_date_aware = tz.localize(part_end_date_naive)
    last_data = get_data("https://api.gdax.com/products/" + currency + "/candles")
    print(last_data)
    # end_date = dateutil.parser.parse(datetime.datetime.fromtimestamp(last_data[-1][0], tz))
    end_date = dateutil.parser.parse(time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(last_data[-1][0])))
    print(end_date)
    url = url_combiner(currency, start_date, part_end_date_naive, granularity)
    print('url: ' + url)
    data = get_data(url)
    data_list = []
    num_of_iter = 0
    while (end_date - start_date) > datetime.timedelta(seconds=12000):
        difference = part_end_date_naive - start_date
        print('The data time period is: ' + str(difference))
        num_of_iter += 1
        print('###########################################################################################')
        print('This is the ' + str(num_of_iter) + ' time iterate')
        while not check_data(data):
            print(start_date)
            part_end_date_naive = part_end_date_naive - datetime.timedelta(seconds=60)
            print(part_end_date_naive)
            url = url_combiner(currency, start_date, part_end_date_naive, granularity)
            print(url)
            data = get_data(url)
        print(url)
        for i in data:
            data_dict = dict(zip(key, i))
            data_dict["time"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(data_dict["time"]))
            data_list.append(data_dict)
        print(str(len(data_list)) + ' records')
        start_date = start_date + difference + datetime.timedelta(seconds=60)
        print(start_date)
        part_end_date_naive = part_end_date_naive + datetime.timedelta(seconds=12060)
        print(part_end_date_naive)
        url = url_combiner(currency, start_date, part_end_date_naive, granularity)
        data = get_data(url)

    for i in last_data:
        last_data_dict = dict(zip(key, i))
        print(last_data_dict)
        last_data_dict["time"] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(last_data_dict["time"]))
        data_list.append(last_data_dict)
    print(data_list)
    return data_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    # help = 'currency start_date (Format: YYYY-MM-DDTHH:MM:SS)'
    parser.add_argument('currency',
                        choices=['LTC-EUR', 'LTC-BTC', 'BTC-GBP', 'BTC-EUR', 'ETH-EUR', 'ETH-BTC', 'LTC-USD',
                                 'BTC-USD', 'ETH-USD'], help='Choose from [\'LTC-EUR\',\'LTC-BTC\', \'BTC-GBP\', '
                                                             '\'BTC-EUR\', \'ETH-EUR\', \'ETH-BTC\', \'LTC-USD\','
                                                             '\'BTC-USD\', \'ETH-USD\']')
    parser.add_argument('start_date')
    args = parser.parse_args()
    c = args.currency
    s_date = dateutil.parser.parse(args.start_date)
    d_list = data_loading(s_date, c)
    save_data(d_list, c)
    print("Data loading complete")
