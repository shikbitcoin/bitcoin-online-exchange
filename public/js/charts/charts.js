/* --------------------------------------------------------
 Flot Charts
 -----------------------------------------------------------*/
//DONE

/* --------------------------------------------------------
 Map
 -----------------------------------------------------------*/
$(function(){
    //USA Map
    if($('#usa-map')[0]) {
	$('#usa-map').vectorMap({
            map: 'us_aea_en',
            backgroundColor: 'rgba(0,0,0,0.25)',
            regionStyle: {
                initial: {
                    fill: 'rgba(255,2552,255,0.7)'
                },
                hover: {
                    fill: '#fff'
                },
            },

            zoomMin:0.88,
            focusOn:{
                x: 5,
                y: 1,
                scale: 1.8
            },
            markerStyle: {
                initial: {
                    fill: '#e80000',
                    stroke: 'rgba(0,0,0,0.4)',
                    "fill-opacity": 2,
                    "stroke-width": 7,
                    "stroke-opacity": 0.5,
                    r: 4
                },
                hover: {
                    stroke: 'black',
                    "stroke-width": 2,
                },
                selected: {
                    fill: 'blue'
                },
                selectedHover: {
                }
            },
            zoomOnScroll: false,

            markers :[
                {latLng: [33, -86], name: 'Sample Name 1'},
                {latLng: [33.7, -93], name: 'Sample Name 2'},
                {latLng: [36, -79], name: 'Sample Name 3'},
                {latLng: [29, -99], name: 'Sample Name 4'},
                {latLng: [33, -95], name: 'Sample Name 4'},
                {latLng: [31, -92], name: 'Liechtenstein'},
            ],
        });
    }

    //World Map
    if($('#world-map')[0]) {
	$('#world-map').vectorMap({
            map: 'world_mill_en',
            backgroundColor: 'rgba(0,0,0,0)',
            series: {
                regions: [{
                    scale: ['#C8EEFF', '#0071A4'],
                    normalizeFunction: 'polynomial'
                }]
            },
            regionStyle: {
                initial: {
                    fill: 'rgba(255,2552,255,0.7)'
                },
                hover: {
                    fill: '#fff'
                },
            },
        });
    }
});


/* --------------------------------------------------------
 Sparkline
 -----------------------------------------------------------*/
 var i =0;
 var BTC = [];
 var ETH = [];
 var LTC = [];

(function getBriefData(){
  jQuery.get( "https://api.gdax.com/products/BTC-USD/candles?start=2017-10-25T00:00:00.22Z&end=2017-10-25T16:00:00.22Z",null,function(data){

    for(i=0;i<data.length;i++){
      BTC.push(Number(data[i][4]));
      i=i-1 + Math.floor(data.length/10);
    }
    console.log(BTC);
    jQuery.get( "https://api.gdax.com/products/ETH-USD/candles?start=2017-10-25T00:00:00.22Z&end=2017-10-25T16:00:00.22Z",null,function(data){

      for(i=0;i<data.length;i++){
        ETH.push(Number(data[i][4]));
        i=i-1 + Math.floor(data.length/10);
      }
      console.log(ETH);
      jQuery.get( "https://api.gdax.com/products/LTC-USD/candles?start=2017-10-25T00:00:00.22Z&end=2017-10-25T16:00:00.22Z",null,function(data){

        for(i=0;i<data.length;i++){
          LTC.push(Number(data[i][4]));
          i=i-1 + Math.floor(data.length/10);
        }
        console.log(LTC);
        },"json" );
      },"json" );
    },"json" );
})();


(function(){
    var BTC1 = new Array(5538.46,5537.92,5474,5399.81,5401.01,5416.99,5511.69,5525.01,5514,5521,5517.31);
    var ETH1 = new Array(296.02,295.98,292.42,290.29,291.01,290.13,296.74,297.6,296.89,296.83,297.32);
    var LTC1 = new Array(54.96,55.32,54.91,53.69,54.48,54.38,55.71,55.74,55.78,56.01,56);
    //Line
    $("#stats-line-2").sparkline(BTC1, {
        type: 'line',
        width: '100%',
        height: '65',
        lineColor: 'rgba(255,255,255,0.4)',
        fillColor: 'rgba(0,0,0,0.2)',
        lineWidth: 1.25,
    });

    $("#stats-line-3").sparkline(ETH1, {
        type: 'line',
        height: '65',
        width: '100%',
        lineColor: 'rgba(255,255,255,0.4)',
        lineWidth: 1.25,
        fillColor: 'rgba(0,0,0,0.2)',
    });

    $("#stats-line-4").sparkline(LTC1, {
        type: 'line',
        height: '65',
        width: '100%',
        lineColor: 'rgba(255,255,255,0.4)',
        lineWidth: 1.25,
        fillColor: 'rgba(0,0,0,0.2)',
    });
})();
