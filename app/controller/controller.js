/**
 * Created by Hangyuan on 2017/10/21.
 */

var BTC_USD_candle_data = require("../models/candle.js").model("BTC_USD_candle_data");
var BTC_EUR_candle_data = require("../models/candle.js").model("BTC_EUR_candle_data");
var BTC_GBP_candle_data = require("../models/candle.js").model("BTC_GBP_candle_data");
var ETH_USD_candle_data = require("../models/candle.js").model("ETH_USD_candle_data");
var ETH_EUR_candle_data = require("../models/candle.js").model("ETH_EUR_candle_data");
var ETH_BTC_candle_data = require("../models/candle.js").model("ETH_BTC_candle_data");
var LTC_USD_candle_data = require("../models/candle.js").model("LTC_USD_candle_data");
var LTC_EUR_candle_data = require("../models/candle.js").model("LTC_EUR_candle_data");
var LTC_BTC_candle_data = require("../models/candle.js").model("LTC_BTC_candle_data");
var request = require('request');
var express = require('express');

/***************************************************
 *         This is the homepage controller.        *
 ***************************************************/
module.exports.showHomepage = function(req,res){
    res.render("index.html");
    console.log("Show the homepage");
};

/***************************************************
 *         This is the map page controller.        *
 ***************************************************/
module.exports.showMapPage = function(req,res){
    res.render("map.html");
    console.log("Show the map page");
};

/***************************************************
 *   These are the real time page controllers.     *
 ***************************************************/
module.exports.showRealTimePage = function(req, res){
    res.render("realtime.html");
    console.log("Show the real time page");
};

module.exports.showBTC_EUR_Page = function(req,res){
    res.render("BTC-EUR.html");
    console.log("Show the BTC-EUR realtime page");
};

module.exports.showBTC_GBP_Page = function(req,res){
    res.render("BTC-GBP.html");
    console.log("Show the BTC-GBP realtime page");
};

module.exports.showETH_USD_Page = function(req,res){
    res.render("ETH-USD.html");
    console.log("Show the ETH-USD realtime page");
};

module.exports.showETH_USD_Page = function(req,res){
    res.render("ETH-USD.html");
    console.log("Show the ETH-USD realtime page");
};

module.exports.showETH_EUR_Page = function(req,res){
    res.render("ETH-EUR.html");
    console.log("Show the ETH-EUR realtime page");
};

module.exports.showETH_BTC_Page = function(req,res){
    res.render("ETH-BTC.html");
    console.log("Show the ETH-BTC realtime page");
};

module.exports.showLTC_USD_Page = function(req,res){
    res.render("LTC-USD.html");
    console.log("Show the LTC-USD realtime page");
};

module.exports.showLTC_EUR_Page = function(req,res){
    res.render("LTC-EUR.html");
    console.log("Show the LTC-EUR realtime page");
};

module.exports.showLTC_BTC_Page = function(req,res){
    res.render("LTC-BTC.html");
    console.log("Show the LTC-BTC realtime page");
};

/***************************************************
 *     These are the history page controllers.     *
 ***************************************************/
module.exports.showHistory_Page = function(req, res){
    res.render("history.html");
    console.log("Show the history page");
};

module.exports.showBTC_EUR_H_Page = function(req, res){
    res.render("BTC-EUR-H.html");
    console.log("Show the BTC-EUR history page");
};

module.exports.showBTC_GBP_H_Page = function(req, res){
    res.render("BTC-GBP-H.html");
    console.log("Show the BTC-GBP history page");
};

module.exports.showETH_USD_H_Page = function(req, res){
    res.render("ETH-USD-H.html");
    console.log("Show the ETH-USD history page");
};

module.exports.showETH_EUR_H_Page = function(req, res){
    res.render("ETH-EUR-H.html");
    console.log("Show the ETH-EUR history page")
};

module.exports.showETH_BTC_H_Page = function(req, res){
    res.render("ETH-BTC-H.html");
    console.log("Show the ETH-BTC history page");
};

module.exports.showLTC_USD_H_Page = function(req, res){
    res.render("LTC-USD-H.html");
    console.log("Show the LTC-USD history page");
};

module.exports.showLTC_EUR_H_Page = function(req, res){
    res.render("LTC-EUR-H.html");
    console.log("Show the LTC-EUR history page");
};

module.exports.showLTC_BTC_H_Page = function(req, res){
    res.render("LTC-BTC-H.html");
    console.log("Show the LTC-BTC history page");
};

/***************************************************
 *   These are the comparison page controllers.    *
 ***************************************************/
module.exports.showComparisonPage = function(req, res){
    res.render("comparison.html");
    console.log("Show the comparison page");
};

module.exports.showEUR_C_Page = function(req, res){
    res.render("EUR-C.html");
    console.log("Show the EUR comparison page");
};

/***************************************************
 *     This is the latest news page controller.    *
 ***************************************************/
module.exports.showLatestPage = function(req, res){
    res.render("latest.html");
    console.log("Show the latest news page");
};

module.exports.BTC_USD_HistoryData = function (req, res) {
    console.log("we are in BTC_USD_HistoryData");
    BTC_USD_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("BTC_USD_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of BTC_USD_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.BTC_EUR_HistoryData = function (req, res) {
    console.log("we are in BTC_EUR_HistoryData");
    BTC_EUR_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("BTC_EUR_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of BTC_EUR_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.BTC_GBP_HistoryData = function (req, res) {
    console.log("we are in BTC_EUR_HistoryData");
    BTC_GBP_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("BTC_GBP_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of BTC_GBP_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.ETH_USD_HistoryData = function (req, res) {
    console.log("we are in ETH_USD_HistoryData");
    ETH_USD_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("ETH_USD_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of ETH_USD_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.ETH_EUR_HistoryData = function (req, res) {
    console.log("we are in BTC_EUR_HistoryData");
    ETH_EUR_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("ETH_EUR_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of ETH_EUR_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.ETH_BTC_HistoryData = function (req, res) {
    console.log("we are in ETH_BTC_HistoryData");
    ETH_BTC_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("ETH_BTC_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of ETH_BTC_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.LTC_USD_HistoryData = function (req, res) {
    console.log("we are in LTC_USD_HistoryData");
    LTC_USD_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("LTC_USD_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of LTC_USD_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.LTC_EUR_HistoryData = function (req, res) {
    console.log("we are in LTC_EUR_HistoryData");
    LTC_EUR_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("LTC_EUR_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of LTC_EUR_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};

module.exports.LTC_BTC_HistoryData = function (req, res) {
    console.log("we are in LTC_BTC_HistoryData");
    LTC_BTC_candle_data.findHistoryData(function(err,result){
        if(err){
            console.log("LTC_BTC_HistoryData Wrong");
        }else{
            console.log(result+"(Blank if result is empty)");
            console.log("Get the result of LTC_BTC_HistoryData");
            console.log("time: " + result[0]["_id"]);
            res.json(result);
        }
    });
};




